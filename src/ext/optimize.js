htmlmerge.optimize = (function() {
    
    var reLineBreaks = /\n{2,}/g,
        reSpaces = /^\s+$/g,
        reDirective = /\[\w*if/i,
        orderingRules = {
            head: {
                start: ['title', 'meta']
            },
        
            body: {
                end: ['script']
            }
        };
    
    
    function _optimizeOrder(el, rules) {
        var child, childToRemove,
            startNodes = [],
            endNodes = [],
            tagName, targetArray;
        
        // iterate through the top level children of the element
        // (we don't want to reorder beyond this level)
        child = el.firstChild;
        while (child) {
            // initialise the tagname
            tagName = (child.tagName || '').toLowerCase();
            
            // reset the targey array to receive the node
            targetArray = null;

            // check for the tag needing to be prioritized at the start
            if (tagName && rules.start && rules.start.indexOf(tagName) >= 0) {
                targetArray = startNodes;
            }
            // or the end...
            else if (tagName && rules.end && rules.end.indexOf(tagName) >= 0) {
                targetArray = endNodes;
            }
            
            // if we have a child to remove, get a reference to it
            childToRemove = targetArray ? child : null;
            
            // get the next child that we will process
            child = child.nextSibling;
            
            // remove the child if this is appropriate
            if (childToRemove) {
                targetArray.push(el.removeChild(childToRemove));
            }
        }
        
        // insert the elements that need to be at the start
        while (startNodes.length) {
            el.insertBefore(startNodes.pop(), el.firstChild);
        }
        
        // insert the elements that should be at the end
        while (endNodes.length) {
            el.appendChild(endNodes.shift());
        }
    }
    
    function _stripEmpty(el) {
        var child = el.firstChild,
            removalQueue = [],
            nodeVal;
        
        while (child) {
            // if we have a comment that contains no ie directives, then remove that too
            if (child instanceof Comment) {
                if (! reDirective.test(child.textContent)) {
                    removalQueue.push(child);
                }
            }
            // if we have text that is empty, then remove it
            else if (child instanceof Text) {
                // get the node val
                nodeVal = (child.data || '').replace(reSpaces, '');
                
                if (! nodeVal) {
                    removalQueue.push(child);
                }
                else {
                    child.data = nodeVal;
                }
            }
            else {
                _stripEmpty(child);
            }
            
            child = child.nextSibling;
        }
        
        // action the removal queue
        while (removalQueue.length) {
            el.removeChild(removalQueue.pop());
        }
    }
    
    return function(doc) {
        ['head', 'body'].forEach(function(tagName) {
            var target = doc.getElementsByTagName(tagName)[0];
            
            // strip empty tags
            _stripEmpty(target);
            
            // optimize the order of tags
            _optimizeOrder(target, orderingRules[tagName]);
        });
        
        return doc;
    };
}());