// req: fs, path, jsdom

var reHTMLContent = /^\s*</,
    Node = jsdom.dom.level3.core.Node,
    Text = jsdom.dom.level3.core.Text,
    Comment = jsdom.dom.level3.core.Comment;

htmlmerge.load = function(filename, opts, callback) {

    function loadIntoDOM(content) {
        callback(null, jsdom.jsdom(content, jsdom.level(3, 'core'), {
            features: {
                QuerySelector: true
            }
        }));
    }

    // if we haven't been provided opts, then deal with that
    if (typeof opts == 'function') {
        callback = opts;
        opts = {};
    }

    // ensure we have opts
    opts = _.extend({}, opts, {
        name: path.basename(filename, path.extname(filename))
    });

    if (reHTMLContent.test(filename)) {
        loadIntoDOM(_.template(filename, opts));
    }
    else {
        fs.readFile(filename, opts.encoding || 'utf8', function(err, content) {
            if (err) return callback(err);
            
            loadIntoDOM(_.template(content, opts));
        });
    }
};