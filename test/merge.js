var mergeCompare = require('./helpers/merge-compare');

describe('merge tests', function() {
    describe('2 way merge', function() {
        it('should be able to merge in a title', mergeCompare('minimal-doc', 'just-a-title'));
        it('should be able to merge a div into the body', mergeCompare('minimal-doc', 'simple-div'));
        it('should be able to insert a div into the body', mergeCompare('minimal-doc', 'simple-div-no-body'));
        it('should be able to combine two documents', mergeCompare('minimal-doc', 'test-doc'));
    });
    
    describe('3 way merge', function() {
        it('should be able to merge three documents', mergeCompare('minimal-doc', 'test-doc', 'just-a-title'));
        it('should be able to merge three documents (order tweak)', mergeCompare('minimal-doc', 'just-a-title', 'test-doc'));
    });
});