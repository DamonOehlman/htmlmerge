var async = require('async'),
    htmlmerge = require('../../'),
    assert = require('assert'),
    jsdom = require('jsdom'),
    fs = require('fs'),
    path = require('path'),
    html = require('html');

module.exports = function() {
    var args = Array.prototype.slice.call(arguments),
        files = args.map.call(arguments, function(filename) {
            return path.resolve(__dirname, '../segments/' + filename + '.html');
        }),
        targetFile = path.resolve(__dirname, '../outputs/', args.join('-') + '.html');
        
    return function(done) {
        var target, output;
        
        // read the target test file
        fs.readFile(targetFile, 'utf8', function(err, baseData) {
            assert.ifError(err, 'No comparison file found: ' + targetFile);

            // open each of the files
            async.map(files, htmlmerge.load, function(err, nodes) {
                assert.ifError(err);
                
                // dirge the nodes down
                target = htmlmerge.apply(null, nodes);

                // get the output
                output = target.innerHTML;

                // assert that the updated target matches what was expected
                assert.equal(output, baseData);
                
                // we are done :)
                done();
            });
        });
    };
};