var mergeCompare = require('./helpers/merge-compare');

describe('real world merge tests', function() {
    it('should be able to merge a simple doc into h5bp', mergeCompare('h5bp', 'test-doc'));
    it('should be able to insert a div into the body', mergeCompare('h5bp', 'simple-div-no-body'));
});