build: node_modules
	@./node_modules/.bin/interleave build --wrap

node_modules:
	npm install

test:
	@mocha --reporter spec