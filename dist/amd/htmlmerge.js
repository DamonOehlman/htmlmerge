/* ~htmlmerge~
 * 
 * Experimental DOM Merge Tool
 * 
 * -meta---
 * version:    0.1.3
 * builddate:  2014-02-20T10:52:04.950Z
 * generator:  interleave@0.5.24
 * 
 * 
 * 
 */ 
define('htmlmerge', ['underscore'], function(_) {
    
    var removableAttributes = ['style'],
        topLevelTags = ['head', 'body'],
        selectorByTagName = _qsaTemplate('<%= tagName %>'),
        selectorById = _qsaTemplate('<%= tagName %>#<%= id %>'),
        selectorByRole = _qsaTemplate('<%= tagName %>[data-role="<%= role %>"]'),
        selectorByClass = _qsaTemplate('<%= tagName %>.<%= className %>'),
    
        // initialise the handlers
        handlerAppendSelf = [
            { selector: selectorByTagName, action: 'copy' }
        ],
        
        handlerReplaceSelf = [
            { selector: selectorById, action: 'replace' },
            { selector: _selectTopLevel, action: 'append' }
        ],
        
        handlerAppendScript = [
            { selector: _selectTopLevel, action: 'append' }
        ],
        
        handlerReplaceSelfUnique = [
            { selector: selectorByTagName, action: 'replace' },
            { selector: _selectTopLevel, action: 'append' }
        ],
        
        handlerReplaceSelfUniqueNoAppend = [
            { selector: selectorByTagName, action: 'replace' }
        ],
        
        handlerSelectSelf = [
            { selector: selectorByTagName, action: 'skip' }
        ],
        
        defaultRules = {
            html:       [],
            head:       handlerSelectSelf,
            body:       handlerSelectSelf,
            title:      handlerReplaceSelfUniqueNoAppend,
            header:     handlerReplaceSelfUnique,
            footer:     handlerReplaceSelfUnique,
            script:     handlerAppendScript
        },
        
        handlerFunctions = {
            append:  _append,
            copy:    _copy,
            replace: _replace
        },
    
        reInvalidChars = /\#$/g;
        
    function _append(targetDoc, targetNode, srcNode) {
        targetNode.appendChild(_duplicate(targetDoc, srcNode));
    }
        
    function _copy(targetDoc, targetNode, srcNode) {
        var child;
        
        child = srcNode.firstChild;
        while (child) {
            targetNode.appendChild(_duplicate(targetDoc, child));
            child = child.nextSibling;
        }
    }
        
    function _duplicate(targetDoc, node) {
        var ii = 0,
            attrCount = node.attributes ? node.attributes.length : 0,
            toRemove = [];
        
        // remove empty (removable attributes) from the node
        for (ii = 0; ii < attrCount; ii++) {
            if ((! node.attributes[ii].value) && removableAttributes.indexOf(node.attributes[ii].name) >= 0) {
                toRemove.push(node.attributes[ii].name);
            }
        }
        
        // remove the attributes that should be
        for (ii = 0; ii < toRemove.length; ii++) {
            node.removeAttribute(toRemove[ii]);
        }
        
        return targetDoc.importNode(node, true);
    }
    
    function _qsaTemplate(templateSelector) {
        var template = _.template(templateSelector);
        
        return function(scope, node) {
            var nodeData = _.extend({}, node, {
                    id: ''
                }),
                selector;
                
            // insert attribute values into the node data
            for (var ii = 0; ii < node.attributes.length; ii++) {
                if (node.attributes[ii].value) {
                    nodeData[node.attributes[ii].name] = nodeData[node.attributes[ii].value];
                }
            }
            
            // initialise the selector
            selector = template(nodeData).replace(reInvalidChars, '');
            // console.log(scope.tagName, selector, scope.querySelectorAll(selector).length);
            
            return scope.querySelectorAll(selector)[0];
        };
    }
    
    function _replace(targetDoc, targetNode, srcNode) {
        while (targetNode.firstChild) {
            targetNode.removeChild(targetNode.firstChild);
        }
        
        _copy(targetDoc, targetNode, srcNode);
    }
    
    function _selectTopLevel(scope, node) {
        var selector, testNode = node;
        
        // walk from the current node to the top and determine whether we should be
        // inserting into the body or the doc head
        while (testNode && topLevelTags.indexOf((testNode.tagName || '').toLowerCase()) < 0) {
            testNode = testNode.parentNode;
        }
        
        // update the selector
        selector = (testNode ? testNode.tagName : '') || 'body';
        
        // console.log('searching top level: ' + selector);
        
        // return the match within the target document
        return scope.querySelectorAll(selector)[0];
    }
    
    function walk(scope, node, rules) {
        var tagName = (node.tagName || '').toLowerCase(),
            actions = rules[tagName] || handlerReplaceSelf,
            selectedAction,
            abortWalk = false,
            bestMatch,
            currentChild;
            
        // attempt to apply the actions in priority order
        for (var ii = 0; tagName && (!bestMatch) && ii < actions.length; ii++) {
            var selector = actions[ii].selector;
    
            // if the selector is a template function, then this needs to be converted to a string
            if (typeof selector == 'function') {
                bestMatch = selector(scope.current[0], node);
            }
            else {
                // use query selector all as this behaves consistently (pretty much) across the 
                // browser and js-dom implementation
                bestMatch = scope.current[0].querySelectorAll(selector)[0];
            }
            
            
            // if we have a bestmatch, update the selected action
            if (bestMatch) {
                selectedAction = actions[ii].action;
            }
        }
        
        // push the best match onto the scope
        if (bestMatch) {
            // if we have a string action then process it
            if (typeof selectedAction == 'string' || (selectedAction instanceof String)) {
                selectedAction = handlerFunctions[selectedAction];
            }
    
            // if we have an action handler, then call it
            if (typeof selectedAction == 'function') {
                abortWalk = selectedAction(scope.doc, bestMatch, node);
            }
    
            // if we have a false action result, then abort the walk
            if (typeof abortWalk == 'undefined' || abortWalk) return;
    
            // add the best match to the current scope
            scope.current.unshift(bestMatch);
        }
        
        // walk through the child nodes
        currentChild = node.firstChild;
        while (currentChild) {
            walk(scope, currentChild, rules);
            
            currentChild = currentChild.nextSibling;
        }
        
        // pop the best match off
        if (bestMatch) {
            scope.current.shift();
        }
    }
    
    function htmlmerge() {
        var nodes = Array.prototype.slice.call(arguments),
            target = nodes.length ? nodes.shift() : null,
            src,
            opts = {};
    
        if (! target) {
            throw new Error('no nodes to merge');
        }
            
        // see if the last argument is an options object (i.e. not a Node)
        if (nodes.length && (! (nodes[nodes.length - 1] instanceof Node))) {
            opts = nodes.pop();
        }
        
        // ensure options are defined
        opts.rules = _.extend({}, defaultRules, opts.rules);
            
        // ensure the target is a dom element node
        target = (target || {}).document || target;
    
        // while we have nodes to process, merge the current node into the target
        src = nodes.shift();
        while (target && src) {
            // iterate through the source tree and work out whether we should do something with it
            walk({ doc: target, current: [target] }, (src || {}).document || src, opts.rules);
            
            // get the next source node
            src = nodes.shift();
        }
        
        return htmlmerge.optimize(target);
    }
    
    htmlmerge.optimize = (function() {
        
        var reLineBreaks = /\n{2,}/g,
            reSpaces = /^\s+$/g,
            reDirective = /\[\w*if/i,
            orderingRules = {
                head: {
                    start: ['title', 'meta']
                },
            
                body: {
                    end: ['script']
                }
            };
        
        
        function _optimizeOrder(el, rules) {
            var child, childToRemove,
                startNodes = [],
                endNodes = [],
                tagName, targetArray;
            
            // iterate through the top level children of the element
            // (we don't want to reorder beyond this level)
            child = el.firstChild;
            while (child) {
                // initialise the tagname
                tagName = (child.tagName || '').toLowerCase();
                
                // reset the targey array to receive the node
                targetArray = null;
    
                // check for the tag needing to be prioritized at the start
                if (tagName && rules.start && rules.start.indexOf(tagName) >= 0) {
                    targetArray = startNodes;
                }
                // or the end...
                else if (tagName && rules.end && rules.end.indexOf(tagName) >= 0) {
                    targetArray = endNodes;
                }
                
                // if we have a child to remove, get a reference to it
                childToRemove = targetArray ? child : null;
                
                // get the next child that we will process
                child = child.nextSibling;
                
                // remove the child if this is appropriate
                if (childToRemove) {
                    targetArray.push(el.removeChild(childToRemove));
                }
            }
            
            // insert the elements that need to be at the start
            while (startNodes.length) {
                el.insertBefore(startNodes.pop(), el.firstChild);
            }
            
            // insert the elements that should be at the end
            while (endNodes.length) {
                el.appendChild(endNodes.shift());
            }
        }
        
        function _stripEmpty(el) {
            var child = el.firstChild,
                removalQueue = [],
                nodeVal;
            
            while (child) {
                // if we have a comment that contains no ie directives, then remove that too
                if (child instanceof Comment) {
                    if (! reDirective.test(child.textContent)) {
                        removalQueue.push(child);
                    }
                }
                // if we have text that is empty, then remove it
                else if (child instanceof Text) {
                    // get the node val
                    nodeVal = (child.data || '').replace(reSpaces, '');
                    
                    if (! nodeVal) {
                        removalQueue.push(child);
                    }
                    else {
                        child.data = nodeVal;
                    }
                }
                else {
                    _stripEmpty(child);
                }
                
                child = child.nextSibling;
            }
            
            // action the removal queue
            while (removalQueue.length) {
                el.removeChild(removalQueue.pop());
            }
        }
        
        return function(doc) {
            ['head', 'body'].forEach(function(tagName) {
                var target = doc.getElementsByTagName(tagName)[0];
                
                // strip empty tags
                _stripEmpty(target);
                
                // optimize the order of tags
                _optimizeOrder(target, orderingRules[tagName]);
            });
            
            return doc;
        };
    }());
    
    
    return typeof htmlmerge != 'undefined' ? htmlmerge : undefined;
});